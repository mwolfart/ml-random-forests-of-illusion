'''
        infos = []
        for attr, _ in dataset[0].items():
            if attr != self.outcome:
                infos.append((attr, self.countOutcomesFromAttributeValuesGroupByOutcome(dataset, attr)))

        selected = ""
        highestGain = 0

        for attr, possibleValuesWithOutcomes in infos:
            # possibleValuesWithOutcomes = {
            #   'youth' : { no: 5, yes: 3, maybe: 0, totalOf: 5}
            #   'middle_aged' : { no: 65, yes: 5, maybe: 2, totalOf: 5}
            #   ...
            # }
            infoOfKey = 0

            for _, outcomesPerValue in possibleValuesWithOutcomes.items():
                # outcomesPerValue = { no: 5, yes: 3, maybe: 0, totalOf: 5}
                infoDj = 0 
                for _, amount in outcomesPerValue.items():
                    infoDj += amount/outcomesPerValue['totalOf'] * log2(amount/outcomesPerValue['totalOf'])
                    # no need to remove totalOf since it will evaluate to zero
                
                infoOfKey += outcomesPerValue['totalOf']/totalOfEntries * -infoDj
            
            gain = infoD - infoOfKey
            if gain >= highestGain:
                selected = attr
                highestGain = gain    

        return selected
        '''
import random
import copy
from math import ceil, floor
import argparse

#Returns a bootstrap dataset
#if mode = "Validation", returns (dataset, validationset)
def genBootstrap(dataset, mode="No Validation"):
    data = copy.deepcopy(dataset)
    total = len(data)
    validationSet = []
    numOfvalidationEntries = random.randint(floor(0.2 * total), ceil(0.5 * total)) # 20% - 50% of data

    # select entries to put into validation set
    # [x1, x2, x3, x4, x5, x6, x7]
    # numOfvalidationEntries = 3

    i = numOfvalidationEntries
    if mode == "Validation":
        while i > 0:
            entryToSelect = random.randint(0,total-1)
            validationSet.append(data[entryToSelect])
            del data[entryToSelect]
            total -= 1
            i -= 1
    

    # validationSet = [x3, x5, x7]
    # data = [x1, x2, x4, x6]

    # replicate elements in remaning training set in order to have the initial amount of entries

    bootstrap = []
    for i in range(len(dataset)):
        entryToSelect = random.randint(0,total-1)
        bootstrap.append(data[entryToSelect])


    # data [x1, x2, x4, x6, xN, xM, xO]

    if len(validationSet) == 0:
        return bootstrap
    else:
        return (bootstrap, validationSet)

def genMultipleBootstraps(data, n):
    bootstraps = []
    # bootstraps = [
    #  (trainingSet1, testSet1),
    #  (trainingSet2, testSet2),
    #  ...
    # ]
    for _ in range(n):
        bootstraps.append(genBootstrap(data))
    return bootstraps
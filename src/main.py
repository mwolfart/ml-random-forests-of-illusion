import reader
from math import floor

try:
    import matplotlib.pyplot as plt
except:
    print ("matplotlib.pyplot not found, graphs won't be plotted")
    print ("To install it, try using python -m pip install -U matplotlib")
    plottable = False
else:
    plottable = True
'''
import tree
import bootstrap
import cross
'''
import random_forest
import forests
import sys
import random

def main():
    SEED = random.randint(1, 99999999)
    print("Using seed:", SEED)

    data, metadata = reader.genDataFromCsv("../data/cancer.data")
    nFold = 10       #Number of Folds for CrossValidation
    nBootstraps = [1,2,3,5,7,10,13,18,21,35] #Number of bootstraps
    # data: [
    #   {'age': 'senior', 'income': 5},
    #   ...
    # ]
    forests = []
    averages = []
    for i, bootstrap in enumerate(nBootstraps):
        forests.append(random_forest.startForest(data, metadata, nFold, bootstrap, SEED))
        averages.append(forests[i].getAverageMeasure())
        forests[i].printForestsMeasures()
        print("A média é:", forests[i].getAverageMeasure())
    if plottable:
        plt.plot(nBootstraps, averages)
        plt.axis('equal')
        plt.ylabel('F1Measure Average')
        plt.show()

if __name__ == '__main__':
    main()

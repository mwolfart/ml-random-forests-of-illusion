from utils import arr_getGreatestElementIndex, dict_incKey, dict_getMostFrequentResult

class ForestGroup:
    def __init__(self):
        self.forests = []
        self.f1measures = []
    def pushForest(self, forest):
        self.forests.append(forest)
        self.f1measures.append(forest.getf1Measure())
    def printForestsMeasures(self):
        print(self.f1measures)
    def getBestForestsMeasures(self):
        return arr_getGreatestElementIndex(self.f1measures)
    def getAverageMeasure(self):
        total = 0
        for measure in self.f1measures:
            total += measure
        return total/len(self.f1measures)
        
class Forest:
    def __init__(self, validationSet, outcomeClass):
        self.forest = []
        
        self.validationSet = validationSet
        self.outcomeClass = outcomeClass

    def pushTree(self, newTree):
        self.forest.append(newTree)
    
    def getf1Measure(self):
        outcomesPossibilities = self.forest[0].getAttributePossibleValues(self.outcomeClass)
        f1 = 0
        for outcome in outcomesPossibilities:
            outcomes = calculateOutcomes(self, self.validationSet, self.outcomeClass, outcome)    # validate
            f1 += f1Measure(outcomes)
        return f1/len(outcomesPossibilities)

    def predictOutcomeForNewEntry(self, entry):
        outcomes = {}
        
        for tree in self.forest:
            result = tree.predictOutcomeForNewEntry(entry)
            dict_incKey(outcomes, result)
        
        return dict_getMostFrequentResult(outcomes)

def calculateOutcomes(tree, testSet, outcomeClass, positiveValue):
    outcomes = {'VP': 0, 'FP': 0, 'VN': 0, 'FN': 0}

    for testEntry in testSet:
        predictedOutcome = tree.predictOutcomeForNewEntry(testEntry)
        realOutcome = testEntry[outcomeClass]

        if (predictedOutcome == positiveValue and realOutcome == positiveValue):
            outcomes['VP'] += 1
        elif (predictedOutcome == positiveValue and realOutcome != positiveValue):
            outcomes['FP'] += 1
        elif (predictedOutcome != positiveValue and realOutcome == positiveValue):
            outcomes['FN'] += 1
        else:
            outcomes['VN'] += 1
        
    return outcomes        

def f1Measure(outcomes):
    beta = 1
    prec = precision(outcomes)
    rev = recall(outcomes)
    if prec == 0 and rev == 0: return 0
    return (1+beta**2) * (prec * rev / (beta**2 * prec + rev))

def precision(outcomes):
    if (outcomes['VP'] == 0 and outcomes['FP'] == 0):
        return 1
    else: return outcomes['VP']/(outcomes['VP']+outcomes['FP'])

def recall(outcomes):
    if (outcomes['VP'] == 0 and outcomes['FN'] == 0):
        return 1
    else: return outcomes['VP']/(outcomes['VP']+outcomes['FN'])
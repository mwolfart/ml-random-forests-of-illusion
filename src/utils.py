def isFloat(n):
    try:
        float(n)
        return True
    except:
        return False

def dict_incKey(dict_, key):
    if key not in dict_:
        dict_[key] = 0
    dict_[key] += 1

def dict_appendKey(dict_, key, value):
    if key not in dict_:
        dict_[key] = []
    dict_[key].append(value)

def dict_appendKeyUnique(dict_, key, value):
    if key not in dict_:
        dict_[key] = []
    if value not in dict_[key]:
        dict_[key].append(value)

def dict2D_incKey(dict_, line, column):
    if line not in dict_:
        dict_[line] = {'totalOf': 0}
    if column not in dict_[line]:
        dict_[line][column] = 0
    dict_[line][column] += 1
    dict_[line]['totalOf'] += 1

def dict_getMostFrequentResult(dict_):
    mostFrequent = 0
    result = ""
    for key, item in dict_.items():
        if item > mostFrequent:
            mostFrequent = item
            result = key
    return result


def listsContainSameElements(list1, list2):
    l1_values = {}
    for x in list1:
        l1_values[x] = 1
    for x in list2:
        if x in l1_values:
            l1_values[x] = 0
        else: return False
    for _, v in l1_values.items():
        if v == 1: return False
    return True

def arr_getGreatestElementIndex(arr):
    if len(arr) == 0:
        return -1
    highestValueIndex = 0
    for x in range(len(arr)):
        if (arr[x] > arr[highestValueIndex]):
            x = highestValueIndex
    return highestValueIndex


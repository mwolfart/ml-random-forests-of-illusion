import re
from utils import dict_appendKeyUnique

def genDataFromCsv(csv):
    data = []
    try:
        with open(csv, "r") as f:
            content = f.readlines()
    except IOError:
        print("ERROR: Unable to open file.")
        return

    attributes = re.split(';|,|\n', content[0])[:-1] 
    for line in content[1:]:
        datum = {}
        values = re.split(";|,|\n", line)[:-1]
        for i, attr in enumerate(attributes):
            datum[attr] = values[i]
        data.append(datum)

    metadata = getMetadata(data)
    metadata['outcome'] = attributes[-1]
    return data, metadata # returns dataset, outcome class and metadata

def getMetadata(dataset):
    metadata = {}
    # { 
    #   'age' : ['youth', 'middle-aged', 'senior'],
    #   ...
    #   'buys_computer' : ['yes', 'no']
    #   'outcome': 'buys_computer'
    # }
    for entry in dataset:
        for attribute in entry.keys():
            dict_appendKeyUnique(metadata, attribute, entry[attribute])
    return metadata

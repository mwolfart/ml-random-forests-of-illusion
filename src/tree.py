import copy
from utils import isFloat, dict_appendKey, dict2D_incKey, dict_incKey, listsContainSameElements
from math import log2, sqrt, ceil
import random

class TreeNode(object):
    def __init__(self, attr, children):
        self.attr = attr
        self.children = children

    def printTree(self, treeNode, tabs=""):
        print("CLASS",treeNode.attr)
        for key,children in treeNode.children.items():
            print(tabs, key, end = " : ")
            if isinstance(children, TreeNode):
                self.printTree(children, tabs + "   ")
            else:
                print(tabs, "LEAF ", children)


class Tree(object):
    def __init__(self, dataset, metadata, bootstrapDivisorStrategy="Disabled", seed=1):
        self.seed = seed
        self.metadata = metadata
        self.outcome = metadata['outcome']
        self.numericalAttributesMeans = self.calcMeansForNumericalAttributes(dataset)
        self.bootstrapDivisorStrategy = bootstrapDivisorStrategy
        # numericalAttributesMeans = {
        #   'age' : 23.5
        #   'weight': 86.43
        #   ...
        # }
        self.root = self.buildTreeFromArray(dataset)

    # Recursive function to build the tree based on a given dataset 
    def buildTreeFromArray(self, dataset):
        if len(dataset) == 0:
            # Should not happen.
            print("ERROR: Dataset must not be empty when building a tree.")
            return
        elif self.allDataHaveSameOutcome(dataset):
            # All have same outcome -> leaf node. Return outcome.
            return dataset[0][self.outcome]
        elif len(dataset[0].keys()) == 1:
            # Only one attribute left (and that is the outcome) -> leaf node; return majority of values
            return self.getMajorityOfOutcomes(dataset)    
        
        selectedAttr = self.selectAttributeToSplit(dataset)
        newNode = TreeNode(selectedAttr, {})
        # newNode = { attr: 'color' , children: {
        #   'red' : TreeNode
        #   'yellow' : TreeNode
        # }}

        splitData = self.splitDataByCategories(dataset, selectedAttr)
        if self.containsCategoryWithoutEntries(selectedAttr, splitData):
            return self.getMajorityOfOutcomes(dataset)
        else:
            # recurse
            for category, data in splitData.items():
                newNode.children[category] = self.buildTreeFromArray(data)

        return newNode

    def splitDataByCategories(self, dataset, attr):
        splitData = {}
        
        # splitData = {
        #   'red' : [entry2, entry5, entry3, ...]
        #   'yellow' : [entry1, entry6, ...]
        # }

        for entry in dataset:
            entryCategory = entry[attr]
            if self.isNumerical(attr):
                entryCategory = self.classifyNumericalValue(attr, entryCategory)     
            
            newEntry = copy.deepcopy(entry)
            del newEntry[attr]
            dict_appendKey(splitData, entryCategory, newEntry)

        return splitData

    # Given a dataset, select the attribute that, when split, produce the most entropy value
    def selectAttributeToSplit(self, dataset):
        attributes = self.preSelectAttributes(dataset)
        gains = []
        for attribute in attributes:
            gains.append((attribute, self.gain(dataset, attribute)))

        highestGain = 0
        selected = ""
        for attribute, gain in gains:
            if gain >= highestGain: 
                highestGain = gain
                selected = attribute
        
        ### print("Selected", selected, "\n")
        return selected
    
    def preSelectAttributes(self, dataset):
        attributes = list(dataset[0].keys())[:-1]

        #Selects the best tree from random choices (bootstrap)
        if self.bootstrapDivisorStrategy == "Root": # m strategy
            length = len(attributes)
            toBeDeleted = length - ceil(sqrt(length))

            while toBeDeleted > 0:
                random.seed(self.seed)
                entryToDelete = random.randint(0,length-1)
                del attributes[entryToDelete]
                length -= 1
                toBeDeleted -= 1
        return attributes

    # Info(D) function, where D = dataset
    def info(self, dataset):
        totalOfEntries = len(dataset)
        if totalOfEntries == 0:
            return 0

        outcomeAmountsByValue = self.countAttributeValuesGroupByValue(dataset, self.outcome)
        # outcomeAmountsByValue = {
        #   'yes' : 5,
        #   'no' : 4,
        #   'maybe' : 6
        # }

        info = 0
        for _, amount in outcomeAmountsByValue.items():
            info += (amount/totalOfEntries) * log2(amount/totalOfEntries)
        return -info

    # Info_a(D) function, where D = dataset, a = attribute
    def info_a(self, attr, dataset):
        possibleValues = self.getAttributePossibleValues(attr)

        sum = 0
        for value in possibleValues:
            Dj = [entry for entry in dataset if entry[attr] == value]
            sum += len(Dj) / len(dataset) * self.info(Dj)
        return sum

    # Gain(A) function
    def gain(self, dataset, attribute):
        gain = self.info(dataset) - self.info_a(attribute, dataset)
        ### print("Attribute", attribute, "- info. gain", gain)
        return gain

    # Attribute counting functions
    def getAttributePossibleValues(self, attribute):
        return self.metadata[attribute]
        #return self.countAttributeValuesGroupByValue(dataset, attribute).keys()

    def countAttributeValuesGroupByValue(self, dataset, attribute):
        if self.isNumerical(attribute):
            return self.countNumericalAttributeValues(dataset, attribute)
        else: return self.countCategoricalAttributeValues(dataset, attribute)

    def countNumericalAttributeValues(self, dataset, attribute):
        attributeValuesCounter = {}
        for entry in dataset:
            valIsLess = entry[attribute] < self.numericalAttributesMeans[attribute]
            if valIsLess: dict_incKey(attributeValuesCounter, 'less')
            else: dict_incKey(attributeValuesCounter, 'geq')
        return attributeValuesCounter

    def countCategoricalAttributeValues(self, dataset, attribute):
        attributeValuesCounter = {}
        for entry in dataset:
            dict_incKey(attributeValuesCounter, entry[attribute])
        return attributeValuesCounter

    # Count outcomes from each attribute
    def countOutcomesFromAttributeValuesGroupByOutcome(self, dataset, attribute):
        possibleValuesAndOutcomes = {}
        # possibleValuesAndOutcomes = {
        #   'youth' : { no: 5, yes: 3, maybe: 0, totalOf: 5}
        #   'middle_aged' : { no: 65, yes: 5, maybe: 2, totalOf: 5}
        #   ...
        # }

        for entry in dataset:
            entryAttrValue = entry[attribute]
            entryOutcome = entry[self.outcome]

            if self.isNumerical(attribute):
                entryAttrValue = self.classifyNumericalValue(attribute, entryAttrValue)            

            dict2D_incKey(possibleValuesAndOutcomes, entryAttrValue, entryOutcome)
        return possibleValuesAndOutcomes

    # Check if a data split by categories contain all categories specified in metadata
    def containsCategoryWithoutEntries(self, attribute, dataDict):
        if self.isNumerical(attribute):
            return not ('less' in dataDict and 'geq' in dataDict)
        else:
            possibleValues = self.getAttributePossibleValues(attribute)
            foundValues = list(dataDict.keys())
            return not listsContainSameElements(foundValues, possibleValues)

    # Test if all data within a dataset contain the same outcome
    def allDataHaveSameOutcome(self, dataset):
        sameOutcomes = True
        prevOutcome = dataset[0][self.outcome]
        for entry in dataset[1:]:
            if (not entry[self.outcome] == prevOutcome):
                sameOutcomes = False
                break
        return sameOutcomes

    # Given a set of data, return the majority of the outcomes in that set
    def getMajorityOfOutcomes(self, dataset):
        outcomes = {}
        maxOutcome = {"value": "", "amount": 0}
        for entry in dataset:
            outcome = entry[self.outcome]
            dict_incKey(outcomes, outcome)

            if outcomes[outcome] > maxOutcome["amount"]:
                maxOutcome = {"value": outcome, "amount": outcomes[outcome]}
        return maxOutcome["value"]

    # Calculate the means for all numerical attributes
    def calcMeansForNumericalAttributes(self, dataset):
        means = {}
        attributes = dataset[0].keys()
        for attribute in attributes:
            mean = self.getAttributeMean(dataset, attribute)
            # not a number: skip attribute
            if not isFloat(mean):
                continue
            else:
                means[attribute] = mean
        return means

    def getAttributeMean(self, dataset, attribute):
        possibleValues = []
        sum = 0
        for entry in dataset:
            value = entry[attribute]

            # not numerical: no mean to calculate
            if not isFloat(value):
                return

            if value not in possibleValues:
                possibleValues.append(value)
                sum += float(value)

        # If attribute possesses more than 5 different numerical values, it is considered numerical.
        if len(possibleValues) >= 5:
            return sum/len(dataset)
        else: return

    # Given a numerical value for an attribute, return if it's less or greater than the mean of that attribute
    def classifyNumericalValue(self, attribute, value):
        if float(value) < self.numericalAttributesMeans[attribute]:
            return 'less'
        else: return 'geq'

    # Given an attribute, check if it's numerical
    def isNumerical(self, attribute):
        return attribute in self.numericalAttributesMeans

    # Given a new entry, predict its outcome
    def predictOutcomeForNewEntry(self, entry):
        currentNode = self.root
        while isinstance(currentNode, TreeNode):
            value = entry[currentNode.attr] # entry['age'] = middle-aged / youth / senior
            if self.isNumerical(currentNode.attr):
                value = self.classifyNumericalValue(currentNode.attr, value)
            currentNode = currentNode.children[value]
        return currentNode

# ....................................
# ............############............
# ........####............####........
# ......##....................##......
# ....##........##....##........##....
# ....##........##....##........##....
# ..##..........##....##..........##..
# ..##......##............##......##..
# ..##......##....####....##......##..
# ..##......################......##..
# ..##........####....####........##..
# ..####........................####..
# ....############################....
# ..........##////////////##..........
# ......######////////////######......
# ........####################........
# ....................................
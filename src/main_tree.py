import reader
import tree
import random

def main():
    SEED = random.randint(1, 99999999)
    print("Using seed:", SEED)

    data, metadata = reader.genDataFromCsv("../data/simpleData.csv")
    tr = tree.Tree(data, metadata, SEED)
    tr.root.printTree(tr.root)

if __name__ == '__main__':
    main()
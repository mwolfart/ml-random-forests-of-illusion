import tree
import bootstrap
import cross
import forests
from utils import dict_incKey, arr_getGreatestElementIndex, dict_getMostFrequentResult
from math import floor

def startForest(dataset, metadata, nFolds, nTrees, bootstrapDivisorStrategy="Root", seed=1):
    outcomeClass = metadata['outcome']
    dataFoldSets = cross.crossValidation(dataset, nFolds)
    
    forestGroup = forests.ForestGroup()
    for trainingData, testData in dataFoldSets:
        forest = forests.Forest(testData, outcomeClass)
        bootstrapSet = bootstrap.genMultipleBootstraps(trainingData, nTrees)
        
        for bstrap in bootstrapSet:
            newTree = tree.Tree(bstrap, metadata, bootstrapDivisorStrategy, seed)
            forest.pushTree(newTree)     
        
        forestGroup.pushForest(forest)
    return forestGroup